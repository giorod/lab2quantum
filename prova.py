import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.sans-serif": ["Helvetica"]})

data1 = pd.read_csv("export.csv")


mean1 = data1['Sum X']
stddev1 = data1['Sum X_square']

print(stddev1/)
