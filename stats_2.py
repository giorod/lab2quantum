import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.sans-serif": ["Helvetica"]})

data1 = pd.read_csv("data_10000_shots.csv")
data2 = pd.read_csv("data_10000_shots_2obs.csv")
data3 = pd.read_csv("data_10000_shots_5obs.csv")
data4 = pd.read_csv("data_10000_shots_10obs.csv")

mean1 = data1['Mean']
stddev1 = data1['StdDev']

mean2 = data2['Mean']
stddev2 = data2['StdDev']

mean3 = data3['Mean']
stddev3 = data3['StdDev']

mean4 = data4['Mean']
stddev4 = data4['StdDev']

xtab = np.arange(0,1000,1)

plt.figure()
plt.title('dV statistics over 100 runs')
plt.xlabel('$N_{iter}$')
plt.ylabel('$|V_{true} - V_{opt}(i)|$')
plt.xlim(0,1000)
plt.ylim(0.0001,1)
plt.grid(axis='both')
plt.plot(xtab,mean1,label='$N=10^4 \: N_{obs} = 1$')
#plt.fill_between(xtab,mean1-stddev1,mean1+stddev1,ls='--',alpha=0.2)
plt.plot(xtab,mean2,label='$N=10^4 \: N_{obs} = 2$')
#plt.fill_between(xtab,mean2-stddev2,mean2+stddev2,alpha=0.2)
plt.plot(xtab,mean3,label='$N=10^4 \: N_{obs} = 5$')
#plt.fill_between(xtab,mean3-stddev3,mean3+stddev3,alpha=0.2)
plt.plot(xtab,mean4,label='$N=10^4 \: N_{obs} = 10$')
#plt.fill_between(xtab,mean4-stddev4,mean4+stddev4,ls='--',alpha=0.2)
plt.legend()
plt.yscale('log')
plt.show()
