# A single SNM step

- Calculate the centroid point
- Calculate the reference point through a reflection -> Always done
- Calculate cost of the reference point -> Always done

3a) Accept reference point if right else:

3b) Expand the reference point and calculate g(x_exp), accept it if right else:

3c) Contract the reference point, calculate g(x_cont), accept else:

3d) Perform ARS

Rinse and repeat

### Variables needed

- Alpha: reflection coefficient
- Beta: contraction coefficient
- Gamma: expansion coefficient

- ARS search if needed.

