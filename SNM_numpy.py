""" Full numpy implementation of NM finite state machine """

import numpy as np

class Point:
    def __init__(self, coords, cost):
        self.coords = coords # List of real values as numpy array
        self.cost = cost # Result of the function evaluation as f(coords)
    
    def __lt__(a,b):
        return a.cost < b.cost

    def __gt__(a,b):
        return a.cost > b.cost
    
    def __str__(self):
        return "Cost: " + str(self.cost) + " / Coords: " + str(self.coords)
    
    def __repr__(self):
        return self.__str__()

class NelderMead:

    # This class works as a finite state machine updating the simplex, the cost calculation is externally made

    REF = 0
    EXP = 1
    INSCONTR = 2
    OUTCONTR = 3
    ARS = 4

    def __init__(self, dimension, points, alpha=1.0, beta=0.5, gamma=2, sigma=0.5):
        
        self.state = NelderMead.REF # state of the minimizer
        
        self.dim = dimension # parameter space dimension
        self.sdim = dimension + 1 # simplex dimension
        self.ALPHA = alpha # reflection parameter
        self.BETA = beta # contraction parameter
        self.GAMMA = gamma # expansion parameter
        self.SIGMA = sigma# shrinking parameter

        self.xref = None # reference point
        self.xexp = None # expansion point
        self.xcontr = None # contraction point
        self.xars = None # adaptive random search point
        self.centr = None # Just a numpy vector

        self.simplex = list(points) # simplex definition
        # add sorting
        self.simplex = sorted(self.simplex, reverse = False) # sorting by cost function




    def get_next_point(self):
        if self.state == NelderMead.REF:
            # calculate centroid and reference point
            self.centr = np.array([0.0]*self.dim)
            for point in self.simplex[:-1]:
                self.centr += point.coords
            self.centr = self.centr/self.dim # centroid calculated excluding the maximum point
            self.xref = Point(((1.0 + self.ALPHA)*self.centr - self.ALPHA*self.simplex[-1].coords), None)
            return self.xref

        elif self.state == NelderMead.EXP:
            # expand reference point
            coordinates = self.GAMMA*self.xref.coords + (1.0 - self.GAMMA)*self.centr
            self.xexp = Point(coordinates, None)
            return self.xexp

        elif self.state == NelderMead.OUTCONTR:
            # contract referring to centroid
            coordinates = self.BETA*self.xref.coords + (1.0 - self.BETA)*self.centr
            self.xref = Point(coordinates, None)
            return self.xref

        elif self.state == NelderMead.INSCONTR:
            # contract referring to maxima
            coordinates = self.BETA*self.simplex[-1].coords + (1.0 - self.BETA)*self.centr
            self.xcontr = Point(coordinates, None)
            return self.xcontr

    def set_next_point(self, newpoint):
        
        # when updating the simplex, we must pop the last element as it has
        # the maximum cost in what would be a p+2 simplex
        # almost done

        if self.state == NelderMead.REF: # step 3a check
            
            self.xref = newpoint

            if newpoint < self.simplex[self.sdim - 2] and newpoint > self.simplex[0]:
                # accept reflection
                self.simplex[self.sdim - 1] = self.xref
                self.simplex = sorted(self.simplex, reverse = False)
            elif newpoint < self.simplex[0]: # expansion needed
                self.state = NelderMead.EXP
            elif newpoint < self.simplex[self.sdim - 1]: # contraction needed
                self.state = NelderMead.OUTCONTR
            else: # we go here if newpoint.cost > simplex.cost_maximum
                self.state = NelderMead.INSCONTR
        
        elif self.state == NelderMead.EXP:
            
            self.xexp = newpoint

            if self.xexp < self.xref:
                self.simplex[self.sdim - 1] = self.xexp
            else:
                self.simplex[self.sdim - 1] = self.xref

            self.state = NelderMead.REF
            self.simplex = sorted(self.simplex, reverse = False)
        
        elif self.state == NelderMead.OUTCONTR:
           
            self.xcontr = newpoint

            if self.xcontr < self.xref:
                self.simplex[self.sdim - 1] = self.xcontr
                self.state = NelderMead.REF
                self.simplex = sorted(self.simplex, reverse = False)
            else:
                for point in self.simplex[1:]:
                    point.coords = self.SIGMA*point.coords + (1.0 - self.SIGMA)*self.simplex[0].coords
                self.state = NelderMead.REF

        elif self.state == NelderMead.INSCONTR:
           
            self.xcontr = newpoint

            if self.xcontr < self.simplex[self.sdim - 1]:
                self.simplex[self.sdim - 1] = self.xcontr
                self.state = NelderMead.REF
                self.simplex = sorted(self.simplex, reverse = False)
            else:
                for point in self.simplex[1:]:
                    point.coords = self.SIGMA*point.coords + (1.0 - self.SIGMA)*self.simplex[0].coords
                self.state = NelderMead.REF
        
        else:
            pass # here we should insert the ARS pattern search
