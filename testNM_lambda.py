### IMPORTS
# Only "nelder_mead" is required for use
import SNM_ARS as nm

# Additional imports used for demonstration + visualization
from qutip import *
import numpy as np
import random
from matplotlib import cm
import matplotlib.pyplot as plt

from smt.sampling_methods import LHS

class DicotomicProjector:
    def __init__(self, direction):
        sigma = [sigmax(),sigmay(),sigmaz()]
        self.dp_plus = 0.5*qeye(2)
        self.dp_minus = 0.5*qeye(2)
        for i in range(3):
            self.dp_plus += 0.5*direction[i]*sigma[i]
            self.dp_minus -= 0.5*direction[i]*sigma[i]
        self.dp = self.dp_plus - self.dp_minus #A = A^+ - A^-
        
class BellEnsemble:
    def __init__(self, p, q=0):
        #Generazione dello stato, se q=0 ho lo stato di Werner al variare di p, altrimenti ho lo stato composto
        self.p = p
        self.q = q
        bell_s = (tensor(basis(2,0),basis(2,1))-tensor(basis(2,1),basis(2,0)))/np.sqrt(2)
        bell_t = (tensor(basis(2,0),basis(2,0))+tensor(basis(2,1),basis(2,1)))/np.sqrt(2)
        self.rho = p*bell_s*bell_s.dag() + (1 - p)*(0.5*q*(bell_s*bell_s.dag() + bell_t*bell_t.dag())+(1-q)*(tensor(qeye(2),qeye(2))/4))

class ExperimentalFunctions:
    def __init__(self, state, N):
        self.state = state
        self.history = []
        self.N = N
        
    def dirvector(self, angles):
        return [np.cos(angles[0])*np.sin(angles[1]),np.sin(angles[0])*np.sin(angles[1]),np.cos(angles[1])]
    
    def exp_evaluation(self, op1, op2, rho, N):
        op1plus = op1.dp_plus
        op1minus = op1.dp_minus
        op2plus = op2.dp_plus
        op2minus = op2.dp_minus
        
        Npp = np.random.poisson(N*((tensor(op1plus,op2plus)*rho).tr()).real)
        Npm = np.random.poisson(N*((tensor(op1plus,op2minus)*rho).tr()).real)
        Nmp = np.random.poisson(N*((tensor(op1minus,op2plus)*rho).tr()).real)
        Nmm = np.random.poisson(N*((tensor(op1minus,op2minus)*rho).tr()).real)
        
        return (Npp-Npm-Nmp+Nmm)/(Npp+Npm+Nmp+Nmm)
    
    def func_free(self, x, num_exp = 1, rho=None, N=None):
        if rho is None:
            rho = self.state.rho
        if N is None:
            N = self.N
        
        #rho è la matrice densità fissata in input
        #calcolo direzioni e operatori
        a0 = self.dirvector(x[0:2])
        a1 = self.dirvector(x[2:4])
        b0 = self.dirvector(x[4:6])
        b1 = self.dirvector(x[6:8])

        A0 = DicotomicProjector(a0)
        A1 = DicotomicProjector(a1)
        B0 = DicotomicProjector(b0)
        B1 = DicotomicProjector(b1)
        
        N00 = 0
        N01 = 0
        N10 = 0
        N11 = 0

        for i in range(num_exp):
            N00 += self.exp_evaluation(A0,B0,rho,N)
            N01 += self.exp_evaluation(A0,B1,rho,N)
            N10 += self.exp_evaluation(A1,B0,rho,N)
            N11 += self.exp_evaluation(A1,B1,rho,N)
        
        #Workaround alla funzione di callback
        #print(N00 + N01 + N10 - N11)
        self.history.append((N00 + N01 + N10 - N11)/num_exp)
        
        return (N00 + N01 + N10 - N11)/num_exp

    def func_nonnoisy(self, x, rho=None):
        #rho è la matrice densità fissata in input
        if rho is None:
            rho = self.state.rho

        a0 = self.dirvector(x[0:2])
        a1 = self.dirvector(x[2:4])
        b0 = self.dirvector(x[4:6])
        b1 = self.dirvector(x[6:8])

        A0 = DicotomicProjector(a0)
        A1 = DicotomicProjector(a1)
        B0 = DicotomicProjector(b0)
        B1 = DicotomicProjector(b1)
        #.real serve perche' qutip da' valore complesso in output
        return ((tensor(A0.dp,B0.dp)*rho).tr() + (tensor(A0.dp,B1.dp)*rho).tr() + (tensor(A1.dp,B0.dp)*rho).tr() - (tensor(A1.dp,B1.dp)*rho).tr()).real

target = [-1.802775636689156, -1.9999999987600998, -2.2022715530026407, -2.408318914920971, -2.617250465400552, -2.828427122521079]
p = [0.5,0.6,0.7,0.8,0.9,1]
N_oss = 10
N = 10000
plt.figure()

for i in range(6):
    xlimits = np.array([[0,3.14],[0,1.57],[0,3.14],[0,1.57],[0,3.14],[0,1.57],[0,3.14],[0,1.57]])
    sampling = LHS(xlimits=xlimits)
    simulationlimit = 1000

    testing = ExperimentalFunctions(BellEnsemble(p[i],0.5),N)

    dimension = 8
    points = []
    x = sampling(dimension + 1)

    cost = testing.func_free

    for item in x:
        exp_point = np.array(item)
        c = cost(exp_point, N_oss)
        points.append(nm.Point(exp_point,c))

    # Initializing the Nelder-Mead model
    model = nm.NelderMead(dimension,points,xlimits)

    #bestintown = []
    #worstintown = []

    k_point = []
    dV = []

    for j in range (0, simulationlimit):
        # 1: Fetch a suggested next point from the model
        point = model.get_next_point()

        # 2 : Compute the result cost of the proposed point
        point.cost = cost(point.coords, N_oss)

        # 3 : Return the point with updated cost back to the model -> simplex might not be updated!
        model.set_next_point(point)

        if (j % 1 == 0):
            k_point.append(j*N_oss)
            dV.append(np.abs(target[i] - testing.func_nonnoisy(model.simplex[0].coords)))
        # add resampling after N iterations?

        #bestintown.append(model.simplex[0].cost)
        #worstintown.append(model.simplex[-1].cost)

    #plt.plot(k_point, dV, '.', ls='-', markevery=100, label="N = "+str(N))
    plt.plot(k_point, dV, ls='-', label="p = "+str(p[i]))

#print(cost(model.simplex[0].coords, 10000))
#print(testing.func_nonnoisy(model.simplex[0].coords))
#print(model)

#plt.figure()
#plt.plot(bestintown)
#plt.plot(worstintown)
plt.title("N=10000, ten observation per proposed point")
plt.ylabel("|V_max - V|")
plt.xlabel("Observations")
plt.yscale("log")
plt.legend()
plt.show()

