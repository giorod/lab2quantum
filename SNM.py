
class Point:
    def __init__(self, coords, cost):
        self.coords = coords # List of real values
        self.cost = cost # Result of the function evaluation as f(coords)
    
    def __lt__(a,b):
        return a.cost < b.cost

    def __gt__(a,b):
        return a.cost > b.cost
    
    def __str__(self):
        return "Cost: " + str(self.cost) + " / Coords: " + str(self.coords)
    
    def __repr__(self):
        return self.__str__()

class VertexCalc:
    @staticmethod
    def centroid(pointlist, dimension):
        sump = [0.0]*dimension
        
        for point in pointlist:
            for i in range(len(sump)):
                sump[i] += point.coords[i]
        
        for i in range(len(sump)):
            sump[i] /= dimension
        
        return sump
            
    @staticmethod
    def generatepoint(a,b,param):
        assert(len(a)==len(b)), "Points have different dimension!"
        return [param*x + (1.0 - param)*y for x,y in zip(a,b)]


class NelderMead:

    # This class works as a finite state machine updating the simplex, the cost calculation is externally made

    REF = 0
    EXP = 1
    INSCONTR = 2
    OUTCONTR = 3
    ARS = 4

    def __init__(self, dimension, points, alpha=1.0, beta=0.5, gamma=2):
        
        self.state = NelderMead.REF
        
        self.dim = dimension
        self.sdim = dimension + 1
        self.ALPHA = alpha
        self.BETA = beta
        self.GAMMA = gamma

        self.xref = None
        self.xexp = None
        self.xcontr = None
        self.xarg = None
        self.centr = None

        self.simplex = list(points)
        # add sorting
        self.simplex = sorted(self.simplex, reverse = False)




    def get_next_point(self):
        VC = VertexCalc
        if self.state == NelderMead.REF:
            # calculate centroid and reference point
            self.centr = Point(VC.centroid(self.simplex[:-1], self.dim), None)
            coordinates = VC.generatepoint(self.simplex[-1].coords,self.centr.coords, -self.ALPHA)
            self.xref = Point(coordinates, None)
            return self.xref

        elif self.state == NelderMead.EXP:
            # expand reference point
            coordinates = VC.generatepoint(self.xref.coords,self.centr.coords, self.GAMMA)
            self.xexp = Point(coordinates, None)
            return self.xexp

        elif self.state == NelderMead.OUTCONTR:
            # contract referring to centroid
            coordinates = VC.generatepoint(self.xref.coords,self.centr.coords, self.BETA)
            self.xref = Point(coordinates, None)
            return self.xref

        elif self.state == NelderMead.INSCONTR:
            # contract referring to maxima
            coordinates = VC.generatepoint(self.xref.coords,self.centr.coords, self.BETA)
            self.xcontr = Point(coordinates, None)
            return self.xcontr

        else:
            # here we should do some kind of ARS
            coords = VC.generatepoint()
            self.xref = Point(coords, None)
            return self.xref

    def set_next_point(self, newpoint):
        
        # when updating the simplex, we must pop the last element as it has
        # the maximum cost in what would be a p+2 simplex
        # almost done

        if self.state == NelderMead.REF: # step 3a check
            
            self.xref = newpoint

            if newpoint < self.simplex[self.sdim - 2] and newpoint > self.simplex[0]:
                # accept reflection
                self.simplex[self.sdim - 1] = self.xref
                self.simplex = sorted(self.simplex, reverse = False)
            elif newpoint < self.simplex[0]: # expansion needed
                self.state = NelderMead.EXP
            elif newpoint < self.simplex[self.sdim - 1]: # contraction needed
                self.state = NelderMead.OUTCONTR
            else: # we go here if newpoint.cost > simplex.cost_maximum
                self.state = NelderMead.INSCONTR
        
        elif self.state == NelderMead.EXP:
            
            self.xexp = newpoint

            if self.xexp < self.xref:
                self.simplex[self.sdim - 1] = self.xexp
            else:
                self.simplex[self.sdim - 1] = self.xref

            self.state = NelderMead.REF
            self.simplex = sorted(self.simplex, reverse = False)
        
        elif self.state == NelderMead.OUTCONTR:
           
            self.xcontr = newpoint

            if self.xcontr < self.xref:
                self.simplex[self.sdim - 1] = self.xcontr
                self.state = NelderMead.REF
                self.simplex = sorted(self.simplex, reverse = False)
            else:
                self.state = ARS

        elif self.state == NelderMead.INSCONTR:
           
            self.xcontr = newpoint

            if self.xcontr < self.simplex[self.sdim - 1]:
                self.simplex[self.sdim - 1] = self.xcontr
                self.state = NelderMead.REF
                self.simplex = sorted(self.simplex, reverse = False)
            else:
                self.state = ARS
        
        else:
            pass # here we should insert the ARS pattern search
