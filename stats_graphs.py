import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.sans-serif": ["Helvetica"]})

data1 = pd.read_csv("/Users/grodari/Desktop/lab2quantum/data_10000shots_pol.csv")
data2 = pd.read_csv("/Users/grodari/Desktop/lab2quantum/data_10000shots_exp.csv")
data3 = pd.read_csv("/Users/grodari/Desktop/lab2quantum/data_10000shots_sinh.csv")

mean1 = data1['Mean']
stddev1 = data1['StdDev']

mean2 = data2['Mean']
stddev2 = data2['StdDev']

mean3 = data3['Mean']
stddev3 = data3['StdDev']
xtab = np.arange(0,1000,1)

plt.figure()
plt.title('dV statistics over 100 runs over singlet state $N=10^4 \: N_{obs} = 1$')
plt.xlabel('$N_{iter}$')
plt.ylabel('$|V_{true} - V_{opt}(i)|$')
plt.xlim(0,1000)
plt.ylim(0.01,1)
plt.grid(axis='both')
plt.plot(xtab,mean1,label='$f(x)=\\frac{x^3}{3} - 2x$')
#plt.fill_between(xtab,mean1-stddev1,mean1+stddev1,ls='--',alpha=0.2)
plt.plot(xtab,mean2,label='$f(x)=\\frac{\\pi}{\\exp{x} + 1}$')
#plt.fill_between(xtab,mean2-stddev2,mean2+stddev2,alpha=0.2)
plt.plot(xtab,mean3,label='$f(x)=sinh(x)$')
#plt.fill_between(xtab,mean3-stddev3,mean3+stddev3,alpha=0.2)
plt.legend()
plt.yscale('log')
plt.show()
